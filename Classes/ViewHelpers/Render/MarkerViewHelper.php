<?php
namespace MED\Medgooglemaps\ViewHelpers\Render;

/**
 * Viewhelper for a Marker on a Map
 *
 * @api
 * @author Simon Häsler <sh@internetgalerie.ch>, Internetgalerie AG
 * @package medgooglemaps
 */ 
class MarkerViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
	/**
	 * Initialize the arguments.
	 *
	 * @return void
	 * @api
	 */
 	public function initializeArguments() {
		parent::initializeArguments();
		
		$this -> registerArgument('address', 'string', 'Address of the marker', false, '');
		$this -> registerArgument('zip', 'string', 'Zip code of the marker', false, '');
		$this -> registerArgument('city', 'string', 'City of the marker', false, '');
		$this -> registerArgument('country', 'string', 'Country of the marker', false, '');
		$this -> registerArgument('latitude', 'double', 'Latitude of the marker', false, 0);
		$this -> registerArgument('longitude', 'double', 'Longitude of the marker', false, 0);
		$this -> registerArgument('infotext', 'string', 'Infotext', false, '');
		$this -> registerArgument('infowindowautoopen', 'bool', 'If the info window should open automatically', false, false);
		$this -> registerArgument('navigation', 'bool', 'If a navigation should appear', false, false);
		$this -> registerArgument('markercolor', 'string', 'Color of the marker', false, 'red');
		$this -> registerArgument('markercustom', 'string', 'Custom marker icon', false, '');
	}
	
	/**
     * @return void
     */
	public function render() {
		$marker = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\\MED\\Medgooglemaps\\Domain\\Model\\Marker');
		
		$marker -> setAddress($this -> arguments['address']);
		$marker -> setZip($this -> arguments['zip']);
		$marker -> setCity($this -> arguments['city']);
		$marker -> setCountry($this -> arguments['country']);
		$marker -> setLatitude(doubleval($this -> arguments['latitude']));
		$marker -> setLongitude(doubleval($this -> arguments['longitude']));
		$marker -> setInfotext($this -> arguments['infotext']);
		$marker -> setInfowindowautoopen(boolval(intval($this -> arguments['infowindowautoopen'])));
		$marker -> setNavigation(boolval(intval($this -> arguments['navigation'])));
		$marker -> setMarkercolor('marker-' . $this -> arguments['markercolor']);
		$marker -> setMarkercustom($this -> arguments['markercustom']);
		
		if($this -> templateVariableContainer -> exists('markers')) {
		  $markers = $this -> templateVariableContainer -> get('markers');
		  $this -> templateVariableContainer -> remove('markers');
		} else {
		  $markers = array();
		}
		
		$markers[] = $marker;
		
		$this -> templateVariableContainer -> add('markers', $markers);
	}
}

?>		
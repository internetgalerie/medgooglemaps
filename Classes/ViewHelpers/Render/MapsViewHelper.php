<?php
namespace MED\Medgooglemaps\ViewHelpers\Render;

/**
 * ViewHelper for checking if a file has a category to set the checkbox
 */
class MapsViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
	/**
	 * @var \MED\Medgooglemaps\Domain\Model\Maps $map
	 */
	protected $map;
	
	public function initializeArguments() {
		parent::initializeArguments();

		$this -> registerArgument('identifier', 'string', 'The unique name of the map', false, 'map');
		$this -> registerArgument('controls', 'array', 'The controls to deactivate', false, array());
		$this -> registerArgument('deactivatePan', 'bool', 'Deactivate pan', false, false);
		$this -> registerArgument('deactivateZoom', 'bool', 'Deactivate zoom', false, false);
		$this -> registerArgument('deactivateMapType', 'bool', 'Deactivate map type', false, false);
		$this -> registerArgument('deactivateScale', 'bool', 'Deactivate scale', false, false);
		$this -> registerArgument('deactivateStreetview', 'bool', 'Deactivate streetview', false, false);
		$this -> registerArgument('deactivateOverview', 'bool', 'Deactivate overview', false, false);
		$this -> registerArgument('deactivateScrolling', 'bool', 'Deactivate scrolling', false, false);
		$this -> registerArgument('markers', 'array', 'The markers on the map', false, null);
		$this -> registerArgument('latitude', 'double', 'The latitude of the map center', false, 0);
		$this -> registerArgument('longitude', 'double', 'The longitude of the map center', false, 0);
		$this -> registerArgument('zoom', 'int', 'The default zoom', false, 0);
		$this -> registerArgument('customStyle', 'bool', 'If the map has a custom style', false, false);
		$this -> registerArgument('customStyleText', 'string', 'The custom style / theme of the map', false, array());
		$this -> registerArgument('style', 'string', 'The style / theme of the map', false, '');
		$this -> registerArgument('mapType', 'string', 'The type of the map', false, 'MapTypeId.ROADMAP');
		$this -> registerArgument('googleMapsSubmitClasses', 'string', 'Classes added to the map', false, 'btn btn-primary');
		$this -> registerArgument('customMarkerPath', 'string', 'The path to the custom marker images', false, array());
		$this -> registerArgument('autocalcmapcenter', 'bool', 'If the map center should be calculated automatically', false, false);
		$this -> registerArgument('aspectRatio', 'string', 'The aspect ratio of the map', false, '');
		$this -> registerArgument('width', 'string', 'The width of the map', false, '450px');
		$this -> registerArgument('height', 'string', 'Height of the map', false, '250px');
		$this -> registerArgument('addJquery', 'bool', 'Add JQuery', false, false);
		$this -> registerArgument('addToFooter', 'bool', 'Add JQuery to footer', false, true);
		$this -> registerArgument('apiUrl', 'string', 'Google Maps API', false, 'https://maps.googleapis.com/maps/api/js?v=3.exp');
		$this -> registerArgument('jsFile', 'string', 'JS File', false, 'typo3conf/ext/medgooglemaps/Resources/Public/Js/functions.js');
		
	}
	
	/**
	 * Renders a google map
	 * 
	 * @return string
	 */
	public function render() {
		$this -> map = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\\MED\\Medgooglemaps\\Domain\\Model\\Maps');

		$this -> renderChildren();
		
		if($this -> templateVariableContainer -> exists('markers')) {
			$markers = $this -> templateVariableContainer -> get('markers');
			$this -> templateVariableContainer -> remove('markers');
		} else {
			$markers = array();
		}
		
		$deactivatePan = boolval($this -> arguments['deactivatePan']);
		$deactivateZoom = boolval($this -> arguments['deactivateZoom']);
		$deactivateMapType = boolval($this -> arguments['deactivateMapType']);
		$deactivateScale = boolval($this -> arguments['deactivateScale']);
		$deactivateStreetview = boolval($this -> arguments['deactivateStreetview']);
		$deactivateOverview = boolval($this -> arguments['deactivateOverview']);
		$deactivateScrolling = boolval($this -> arguments['deactivateScrolling']);
		
		$controls = $this -> arguments['controls'];
		
		if($deactivatePan && !in_array('panControl', $controls))
			$controls[] = 'panControl';
		
		if($deactivateZoom && !in_array('zoomControl', $controls))
			$controls[] = 'zoomControl';
			
		if($deactivateMapType && !in_array('mapTypeControl', $controls))
			$controls[] = 'mapTypeControl';
		
		if($deactivateScale && !in_array('streetViewControl', $controls))
			$controls[] = 'scaleControl';
		
		if($deactivateStreetview && !in_array('streetViewControl', $controls))
			$controls[] = 'streetViewControl';
		
		if($deactivateOverview && !in_array('overviewMapControl', $controls))
			$controls[] = 'overviewMapControl';
		
		if($deactivateScrolling && !in_array('scrollwheel', $controls))
			$controls[] = 'scrollwheel';
		
		$this -> arguments['controls'] = $controls;
		
		$this -> map -> setIdentifier($this -> arguments['identifier'] . '_' . \MED\Medgooglemaps\Domain\Model\Maps::$instanceCount);
		$this -> map -> setControls($this -> arguments['controls']);
		$this -> map -> setMarkers($markers);
		$this -> map -> setLatitude(doubleval($this -> arguments['latitude']));
		$this -> map -> setLongitude(doubleval($this -> arguments['longitude']));
		$this -> map -> setZoom(intval($this -> arguments['zoom']));
		$this -> map -> setCustomStyle(boolval($this -> arguments['customStyle']));
		$this -> map -> setCustomStyleText($this -> arguments['customStyleText']);
		$this -> map -> setStyle($this -> arguments['style']);
		$this -> map -> setMapType($this -> arguments['mapType']);
		$this -> map -> setGoogleMapsSubmitClasses($this -> arguments['googleMapsSubmitClasses']);
		$this -> map -> setCustomMarkerPath($this -> arguments['customMarkerPath']);
		$this -> map -> setAutocalcmapcenter(boolval($this -> arguments['autocalcmapcenter']));
		$this -> map -> setAspectRatio($this -> arguments['aspectRatio']);
		$this -> map -> setWidth($this -> arguments['width']);
		$this -> map -> setHeight($this -> arguments['height']);
		$this -> map -> setAddJquery($this -> arguments['addJquery']);
		$this -> map -> setAddToFooter($this -> arguments['addToFooter']);
		$this -> map -> setApiUrl($this -> arguments['apiUrl']);
		$this -> map -> setJsFile($this -> arguments['jsFile']);
		
		$this -> map -> generateJavaScript();
		
		$identifier = $this -> map -> getIdentifier();
		$aspectRatio = $this -> map -> getAspectRatio();
		$width = $this -> map -> getWidth();
		$height = $this -> map -> getHeight();
		
		// Create the outputstring
		$out = '<div id="'. $identifier . '"';
		
		if($aspectRatio)
			$out .= ' class="map_aspect_ratio_' . $aspectRatio . '"';
		
		$out .= ' style="width: ' . $width;
		
		if(!$aspectRatio)
			$out .= '; height: '. $height;
			
		$out .= ';"></div>';
		
		return $out;
	}
}

<?php
namespace MED\Medgooglemaps\Utility\Hook;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

class WizIcon {
	
	/**
	 * Path to locallang file (with : as postfix)
	 *
	 * @var string
	 */
	protected $locallangPath = 'LLL:EXT:medgooglemaps/Resources/Private/Language/locallang_be.xlf:';
	
	protected $plugins = array('medgooglemaps_maps');
	
	/**
	 * Processing the wizard items array
	 *
	 * @param array $wizardItems
	 * @return array
	 */
	public function proc($wizardItems = array()) {
		foreach ($this->plugins as $plugin)
			$wizardItems['plugins_tx_' . $plugin] = $this->createWizardItem($plugin);
		
		return $wizardItems;
	}
	
	protected function createWizardItem($plugin) {
		return array(
			'icon' => ExtensionManagementUtility::extRelPath('medgooglemaps') . 'Resources/Public/Icons/ExtIcons/' . $plugin . '.png',
			'title' => $GLOBALS['LANG']->sL($this->locallangPath . $plugin . '_pluginWizardTitle', TRUE),
			'description' => $GLOBALS['LANG']->sL($this->locallangPath . $plugin . '_pluginWizardDescription', TRUE),
			'params' => '&defVals[tt_content][CType]=list&defVals[tt_content][list_type]=' . $plugin,
			'tt_content_defValues' => array(
				'CType' => 'list'
			)
		);
	}
}
<?php
namespace MED\Medgooglemaps\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * MapsController
 */
class MapsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	/**
	 * mapsRepository
	 *
	 * @var \MED\Medgooglemaps\Domain\Repository\MapsRepository
	 * @inject
	 */
	protected $mapsRepository = NULL;

	/**
	 * markerRepository
	 *
	 * @var \MED\Medgooglemaps\Domain\Repository\MarkerRepository
	 * @inject
	 */
	protected $markerRepository = NULL;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		// Marker new
		$cObj = $this -> configurationManager -> getContentObject();
		
		if($this -> settings['controls'])
			$controls = explode(',', $this -> settings['controls']);
		else
			$controls = array();
		
		if($this -> settings['marker'])
			$markers = $this -> markerRepository -> setRespectStoragePage(false) -> findByMarkersFromSettings(explode(',', $this -> settings['marker'])) -> toArray();
		else
			$markers = array();
		
		$latitude = doubleval($this -> settings['latitude']);
		$longitude = doubleval($this -> settings['longitude']);
		$zoom = intval($this -> settings['zoom']);
		$customStyle = boolval($this -> settings['customStyle']);
		$customStyleText = $this -> settings['customStyleText'];
		$style = $this -> settings['style'];
		$mapType = $this -> settings['mapType'];
		$googleMapsSubmitClasses = $this -> settings['googleMapsSubmitClasses'];
		$customMarkerPath = $this -> settings['customMarkerPath'];
		$autocalcmapcenter = boolval($this -> settings['autocalcmapcenter']);
		$aspectRatio = $this -> settings['aspectRatio'];
		$width = $this -> settings['width'];
		$height = $this -> settings['height'];
		$addJquery = $this -> settings['addJquery'];
		$addToFooter = $this -> settings['addToFooter'];
		$apiUrl = $this -> settings['apiUrl'];
		$jsFile = $this -> settings['jsFile'];

		// Template vars
		$this -> view -> assign('identifier', 'map_' . $cObj -> data['uid']);
		$this -> view -> assign('controls', $controls);
		$this -> view -> assign('markers', $markers);
		$this -> view -> assign('latitude', $latitude);
		$this -> view -> assign('longitude', $longitude);
		$this -> view -> assign('zoom', $zoom);
		$this -> view -> assign('mapType', $mapType);
		$this -> view -> assign('customStyle', $customStyle);
		$this -> view -> assign('customStyleText', $customStyleText);
		$this -> view -> assign('style', $style);
		$this -> view -> assign('googleMapsSubmitClasses', $googleMapsSubmitClasses);
		$this -> view -> assign('customMarkerPath', $customMarkerPath);
		$this -> view -> assign('autocalcmapcenter', $autocalcmapcenter);
		$this -> view -> assign('aspectRatio', $aspectRatio);
		$this -> view -> assign('width', $width);
		$this -> view -> assign('height', $height);
		$this -> view -> assign('addJquery', $addJquery);
		$this -> view -> assign('addToFooter', $addToFooter);
		$this -> view -> assign('apiUrl', $apiUrl);
		$this -> view -> assign('jsFile', $jsFile);
	}
}

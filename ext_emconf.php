<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "medgooglemaps".
 *
 * Auto generated 16-10-2014 15:36
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Google Maps',
	'description' => 'Google Maps',
	'category' => 'plugin',
	'version' => '0.2.0',
	'state' => 'beta',
	'uploadfolder' => false,
	'createDirs' => '',
	'clearcacheonload' => false,
	'author' => 'Raphael Zschorsch',
	'author_email' => 'rafu1987@gmail.com',
	'author_company' => NULL,
	'constraints' => array (
		'depends' => array (
			'typo3' => '6.2.0-7.9.99',
            'vhs' => ''
		),
		'conflicts' => array (
		),
		'suggests' => array (
		),
	),
);


<?php
namespace MED\Medgooglemaps\Domain\Model;
/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
/**
 * Google Maps
 */
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use RZ\Medgooglemaps\Utility\T3jquery;

class Maps extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
	/**
	 * Instance count, helps uniquely identifying the map-object
	 * 
	 * @var int
	 */
	public static $instanceCount = 0;
	
	/**
	 * Boolean for checking if the javascript is already included to avoid a double-include if multiple maps are on one page
	 * 
	 * @var bool
	 */
	public static $jsIncluded = false;
	
	/**
	 * @var string
	 */
	protected $identifier;
	
	/**
	 * @var array
	 */
	protected $controls;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Med\Medgooglemaps\Domain\Model\Marker>
	 */
	protected $markers;
	
	/**
	 * @var double
	 */
	protected $latitude;
	
	/**
	 * @var double
	 */
	protected $longitude;
	
	/**
	 * @var int
	 */
	protected $zoom;
	
	/**
	 * @var string
	 */
	protected $customStyle;
	
	/**
	 * @var string
	 */
	protected $customStyleText;
	
	/**
	 * @var string
	 */
	protected $style;
	
	/**
	 * @var string
	 */
	protected $mapType;
	
	/**
	 * @var string
	 */
	protected $googleMapsSubmitClasses;
	
	/**
	 * @var string
	 */
	protected $customMarkerPath;
	
	/**
	 * @var bool
	 */
	protected $autocalcmapcenter;
	
	/**
	 * @var string
	 */
	protected $aspectRatio;
	
	/**
	 * @var string
	 */
	protected $width;
	
	/**
	 * @var string
	 */
	protected $height;
	
	/**
	 * @var bool
	 */
	protected $addJquery;
	
	/**
	 * @var bool
	 */
	protected $addToFooter;
	
	/**
	 * @var string
	 */
	protected $apiUrl;
	
	/**
	 * @var string
	 */
	protected $jsFile;
	
	/**
	 * Constructor, increments $instanceCount
	 */
	public function __construct() {
		self::$instanceCount++;
	}
	
	/**
	 * @return string
	 */
	public function getIdentifier() {
		return $this -> identifier;
	}
	
	/**
	 * @return array
	 */
	public function getControls() {
		return $this -> controls;
	}
	
	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Med\Medgooglemaps\Domain\Model\Marker>
	 */
	public function getMarkers() {
		return $this -> markers;
	}
	
	/**
	 * @return double
	 */
	public function getLatitude() {
		return $this -> latitude;
	}
	
	/**
	 * @return double
	 */
	public function getLongitude() {
		return $this -> longitude;
	}
	
	/**
	 * @return int
	 */
	public function getZoom() {
		return $this -> zoom;
	}
	
	/**
	 * @return string
	 */
	public function getCustomStyle() {
		return $this -> customStyle;
	}
	
	/**
	 * @return string
	 */
	public function getCustomStyleText() {
		return $this -> customStyleText;
	}
	
	/**
	 * @return string
	 */
	public function getStyle() {
		return $this -> style;
	}
	
	/**
	 * @return string
	 */
	public function getMapType() {
		return $this -> mapType;
	}
	
	/**
	 * @return string
	 */
	public function getGoogleMapsSubmitClasses() {
		return $this -> googleMapsSubmitClasses;
	}
	
	/**
	 * @return string
	 */
	public function getCustomMarkerPath() {
		return $this -> customMarkerPath;
	}
	
	/**
	 * @return bool
	 */
	public function getAutocalcmapcenter() {
		return $this -> autocalcmapcenter;
	}
	
	/**
	 * @return string
	 */
	public function getAspectRatio() {
		return $this -> aspectRatio;
	}
	
	/**
	 * @return string
	 */
	public function getWidth() {
		return $this -> width;
	}
	
	/**
	 * @return string
	 */
	public function getHeight() {
		return $this -> height;
	}
	
	/**
	 * @return bool
	 */
	public function getAddJquery() {
		return $this -> addJquery;
	}
	
	/**
	 * @return string
	 */
	public function getAddToFooter() {
		return $this -> addToFooter;
	}
	
	/**
	 * @return string
	 */
	public function getApiUrl() {
		return $this -> apiUrl;
	}
	
	/**
	 * @return string
	 */
	public function getJsFile() {
		return $this -> jsFile;
	}
	
	/**
	 * Sets the identifier
	 * 
	 * @param array $identifier
	 * 
	 * @return void
	 */
	public function setIdentifier($identifier) {
		$this -> identifier = $identifier;
	}
	
	/**
	 * Sets the controls
	 * 
	 * @param array $controls
	 * 
	 * @return void
	 */
	public function setControls($controls) {
		$this -> controls = $controls;
	}
	
	/**
	 * Sets the markers
	 * 
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Med\Medgooglemaps\Domain\Model\Marker> $markers
	 * 
	 * @return void
	 */
	public function setMarkers($markers) {
		$this->markers = $markers;
	}
	
	/**
	 * Sets the latitude
	 * 
	 * @param double $latitude
	 * 
	 * @return void
	 */
	public function setLatitude($latitude) {
		$this -> latitude = $latitude;
	}
	
	/**
	 * Sets the longitude
	 * 
	 * @param double $longitude
	 * 
	 * @return void
	 */
	public function setLongitude($longitude) {
		$this -> longitude = $longitude;
	}
	
	/**
	 * Sets the zoom
	 * 
	 * @param int $zoom
	 * 
	 * @return void
	 */
	public function setZoom($zoom) {
		$this -> zoom = $zoom;
	}
	
	/**
	 * Sets the customStyle
	 * 
	 * @param string $customStyle
	 * 
	 * @return void
	 */
	public function setCustomStyle($customStyle) {
		$this -> customStyle = $customStyle;
	}
	
	/**
	 * Sets the customStyleText
	 * 
	 * @param string $customStyleText
	 * 
	 * @return void
	 */
	public function setCustomStyleText($customStyleText) {
		$this -> customStyleText = $customStyleText;
	}
	
	/**
	 * Sets the style
	 * 
	 * @param string $style
	 * 
	 * @return void
	 */
	public function setStyle($style) {
		$this -> style = $style;
	}
	
	/**
	 * Sets the mapType
	 * 
	 * @param string $mapType
	 * 
	 * @return void
	 */
	public function setMapType($mapType) {
		$this -> mapType = $mapType;
	}
	
	/**
	 * Sets the googleMapsSubmitClasses
	 * 
	 * @param string $googleMapsSubmitClasses
	 * 
	 * @return void
	 */
	public function setGoogleMapsSubmitClasses($googleMapsSubmitClasses) {
		$this -> googleMapsSubmitClasses = $googleMapsSubmitClasses;
	}
	
	/**
	 * Sets the customMarkerPath
	 * 
	 * @param string $customMarkerPath
	 * 
	 * @return void
	 */
	public function setCustomMarkerPath($customMarkerPath) {
		$this -> customMarkerPath = $customMarkerPath;
	}
	
	/**
	 * Sets the autocalcmapcenter
	 * 
	 * @param bool $autocalcmapcenter
	 * 
	 * @return void
	 */
	public function setAutocalcmapcenter($autocalcmapcenter) {
		$this -> autocalcmapcenter = $autocalcmapcenter;
	}
	
	/**
	 * Sets the aspectratio
	 * 
	 * @param bool $aspectratio
	 * 
	 * @return void
	 */
	public function setAspectRatio($aspectRatio) {
		$this -> aspectRatio = $aspectRatio;
	}
	
	/**
	 * Sets the width
	 * 
	 * @param string $width
	 * 
	 * @return void
	 */
	public function setWidth($width) {
		$this -> width = $width;
	}
	
	/**
	 * Sets the height
	 * 
	 * @param string $height
	 * 
	 * @return void
	 */
	public function setHeight($height) {
		$this -> height = $height;
	}
	
	/**
	 * Sets the addJquery
	 * 
	 * @param bool $addJquery
	 * 
	 * @return void
	 */
	public function setAddJquery($addJquery) {
		$this -> addJquery = $addJquery;
	}
	
	/**
	 * Sets the addToFooter
	 * 
	 * @param bool $addToFooter
	 * 
	 * @return void
	 */
	public function setAddToFooter($addToFooter) {
		$this -> addToFooter = $addToFooter;
	}
	
	/**
	 * Sets the apiUrl
	 * 
	 * @param string $apiUrl
	 * 
	 * @return void
	 */
	public function setApiUrl($apiUrl) {
		$this -> apiUrl = $apiUrl;
	}
	
	/**
	 * Sets the jsFile
	 * 
	 * @param string $jsFile
	 * 
	 * @return void
	 */
	public function setJsFile($jsFile) {
		$this -> jsFile = $jsFile;
	}
	
	public function generateJavaScript() {
		if(!self::$jsIncluded)
			$this -> includeJavaScript();
		
		if($this -> controls)
			$controlsConfig = $this -> getControlsConfig();

		if($this -> customStyle) {
			$styleOutput = 'styles: ' . $this -> customStyleText . ',';
		} else if($this -> style) {
			$theme = $this -> getPartialView('Theme-' . $this -> style, array(), 'Themes/');
			$styleOutput = 'styles: ' . $theme -> render() . ',';
		}
		
		// Map type
		$mapTypeOutput = $this -> mapType ? $this -> mapType : 'MapTypeId.ROADMAP';

		// Markers
		if($this -> markers) {
			$i = 1;
			
			foreach($this -> markers as $marker) {
				// Clear vars
				$infoTextOutput = '';
				$navigationOutput = '';
				
				if($marker -> getInfotext()) {
					$infoTextOutput = $this -> getInfotextOutput($marker);
					
					if($marker -> getNavigation()) {
						$infoTextOutput .= ',';
						$navigationOutput = $this -> getNavigationOutput($marker);
					}
				}

				$markerOutput .= 'var contentString' . $i . ' = [' . $infoTextOutput . $navigationOutput . '].join("\n");';

				// Locations
				if($marker -> getInfowindowautoopen())
					$infoWindowAutoOpen = $marker -> getInfowindowautoopen();
				else
					$infoWindowAutoOpen = 0;

				if(!$this -> customMarkerPath)
					$this -> customMarkerPath = 'uploads/pics/';

				if($marker -> getMarkercustom())
					$markerCustom = $this -> customMarkerPath . $marker -> getMarkercustom();

				$locationsOutput .= '[
					contentString' . $i . ',
					' . $marker -> getLatitude() . ',
					' . $marker -> getLongitude() . ',
					' . $infoWindowAutoOpen . ',
					"' . $marker -> getMarkercolor() . '",
					"' . $markerCustom . '"
				]';

				if($i < count($this -> markers))
					$locationsOutput .= ',';

				$i++;
			}

			$locationsOutput = 'var locations = [' . $locationsOutput . '];';
        	
			$processMarker = 'var i; 
			for (i = 0; i < locations.length; i++) {';
			
			if($this -> autocalcmapcenter)
				$processMarker .= 'bounds.extend(new google.maps.LatLng(locations[i][1], locations[i][2]));';
			
			$processMarker .= 'if(locations[i][4]) {
                    var markerIcon = "' . ExtensionManagementUtility::siteRelPath('medgooglemaps') . 'Resources/Public/Icons/marker2/" + locations[i][4] + ".png";
                } else {
                    var markerIcon = "";
                }

                if(locations[i][5]) {
                    var markerIcon = locations[i][5];   
                }

                marker[i] = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: markerIcon
                });
                
                if(locations[i][0]) {
                    marker[i].medInfoWindow = new google.maps.InfoWindow({
                        content: locations[i][0],
                        disableAutoPan: true
                    });

                    google.maps.event.addListener(marker[i], "click", function() {
                        // Close previously opened infowindows
                        for (i = 0; i < locations.length; i++) {
                        	if(marker[i].medInfoWindow)
                            	marker[i].medInfoWindow.close();
                        }

                        this.medInfoWindow.disableAutoPan = false;
                        this.medInfoWindow.open(map,this);
                    });
  
                    if(locations[i][3] == 1) {
                        autoOpen.push(i);
                    }
                }
            }';
		}

		$js = '<script type="text/javascript">(function() {
        var map;
        var marker = [];
        var autoOpen = [];

        function initialize() {
            var latLng = new google.maps.LatLng(' . $this -> latitude . ',' . $this -> longitude . ');
            var options = {
                zoom: ' . $this -> zoom . ',
                ' . $styleOutput . '
                center: latLng,
                mapTypeId: google.maps.' . $mapTypeOutput . '
                ' . $controlsConfig . '
            }
            map = new google.maps.Map(document.getElementById("' . $this -> identifier . '"), options);';

		if($this -> autocalcmapcenter)
			$js .= 'var bounds = new google.maps.LatLngBounds();';
			
		$js .= $markerOutput . $locationsOutput . $processMarker;
			
		if($this -> autocalcmapcenter) {
			$js .= 'map.fitBounds(bounds);
			map.panToBounds(bounds);
			var boundsChangedListener = google.maps.event.addListenerOnce(map, \'bounds_changed\', function(event) {
				if(this.getZoom())
					this.setZoom(options.zoom);
				google.maps.event.removeListener(boundsChangedListener);
			});';
		}

		$js .= 'google.maps.event.addListenerOnce(map, "idle", function() {
	            if(autoOpen.length > 0) {
	                for (var i = 0; i < autoOpen.length; i++) {
	                    marker[autoOpen[i]].medInfoWindow.open(map, marker[autoOpen[0]]);
	                }
	            }
	        });
    	}

        google.maps.event.addDomListener(window, "load", initialize);';
		
		$js .= '})();</script>';
		
		$GLOBALS['TSFE'] -> additionalFooterData['medgooglemaps'] .= $js;
	}

	/**
	 * Includes the javascript
	 * 
	 * @return void
	 */
	protected function includeJavaScript() {
		// t3jquery
		$t3jqueryCheck = T3jquery::check();

		// Add jQuery?
		if($t3jqueryCheck === false) {
			if($this -> addJquery) {
				if($this -> addToFooter) {
					$GLOBALS['TSFE'] -> additionalFooterData['medgooglemaps_jquery'] = '<script type="text/javascript" src="' . ExtensionManagementUtility::siteRelPath('medgooglemaps') . 'Resources/Public/Js/jquery-1.11.2.min.js"></script>';
				} else {
					$GLOBALS['TSFE'] -> additionalHeaderData['medgooglemaps_jquery'] = '<script type="text/javascript" src="' . ExtensionManagementUtility::siteRelPath('medgooglemaps') . 'Resources/Public/Js/jquery-1.11.2.min.js"></script>';
				}
			}
		}
		
		// Add JS files
		$GLOBALS['TSFE']->additionalHeaderData['medgooglemaps_api'] = '<script type="text/javascript" src="' . $this -> apiUrl . '"></script>';

		if($this -> addToFooter) {
			$GLOBALS['TSFE'] -> additionalFooterData['medgooglemaps_js'] = '<script type="text/javascript" src="' . ExtensionManagementUtility::siteRelPath('medgooglemaps'). $this -> jsFile . '"></script>';
		} else {
			$GLOBALS['TSFE'] -> additionalHeaderData['medgooglemaps_js'] = '<script type="text/javascript" src="' . ExtensionManagementUtility::siteRelPath('medgooglemaps') . $this -> jsFile . '"></script>';
		}
		
		self::$jsIncluded = true;
	}
	
	/**
	 * Gets the controls configuration
	 * 
	 * @return array
	 */
	protected function getControlsConfig() {
		$controlsConfig = ',';
		
		foreach($this -> controls as $c) {
			if($c == 'scaleControl') {
				$controlsConfig .= $c . ': true,';
			} else {
				$controlsConfig .= $c . ': false,';
			}
		}
		
		return substr($controlsConfig, 0, -1);
	}

	/**
	 * Gets the infotextoutput for a marker
	 * 
	 * @param \MED\Medgooglemaps\Domain\Model\Marker $marker
	 * @return string
	 */
	protected function getInfotextOutput($marker) {
		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TYPO3\CMS\Extbase\Object\ObjectManager');
		$configurationManager =  $objectManager->get('\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface');
		$cObj = $configurationManager->getContentObject();
		
		$parseFunc = $GLOBALS['TSFE'] -> tmpl -> setup['lib.']['parseFunc_RTE.'];
		$infotext = '';
		
		if(is_array($parseFunc))
			$infotext = $cObj -> parseFunc($marker -> getInfotext(), $parseFunc);
		
		//$infotext = \TYPO3\CMS\Frontend\Plugin::pi_RTEcssText($marker->getInfotext(),
		// $cObj);
		$infotext = str_replace(
			array(
				'\r',
				'\n'
			),
			'',
			$infotext
		);
		
		return '\'<div class="medgooglemaps_content">' . $infotext . '</div>\'';
	}

	/**
	 * Gets the navigationoutput for a marker
	 * 
	 * @param \MED\Medgooglemaps\Domain\Model\Marker $marker
	 * @return string
	 */
	protected function getNavigationOutput($marker) {	
		// Config for fluid standalone view for markers
		$confArr = array(
			'settings' => array('googleMapsSubmitClasses' => $this -> googleMapsSubmitClasses),
			'marker' => $marker
		);

		$navigation = $this -> getPartialView('Navigation', $confArr);
		
		return json_encode($navigation -> render());
	}
	
	/**
	 * Creates a standaloneview for a partial
	 * 
	 * @param string $templateName
	 * @param array $variables
	 * @param string $prefix
	 * @return \TYPO3\CMS\Fluid\View\StandaloneView
	 */
	protected function getPartialView($templateName, array $variables = array(), $prefix = '') {
		$partialView = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\\TYPO3\\CMS\\Fluid\\View\\StandaloneView');
		$partialView -> setFormat('html');
		$templateRootPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath('medgooglemaps') . 'Resources/Private/Partials/';
		$templatePathAndFilename = $templateRootPath . $prefix . $templateName . '.html';
		$partialView -> setTemplatePathAndFilename($templatePathAndFilename);
		$partialView -> assignMultiple($variables);
		$partialView -> getRequest() -> setControllerExtensionName('Medgooglemaps'); // This should not be hardcoded, but how?

		return $partialView;
	}
}
